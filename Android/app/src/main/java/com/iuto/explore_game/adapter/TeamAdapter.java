package com.iuto.explore_game.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.iuto.explore_game.MainActivity;
import com.iuto.explore_game.R;
import com.iuto.explore_game.database.Aventure.AventureDAO;
import com.iuto.explore_game.database.Aventure.AventureModel;
import com.iuto.explore_game.database.Member.MemberDAO;
import com.iuto.explore_game.database.Team.TeamDAO;
import com.iuto.explore_game.database.Team.TeamModel;
import com.iuto.explore_game.login.LoginActivity;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.ViewHolder> {
    private final int layout_id;
    private final List<TeamModel> listeTeam;
    private final Activity mainActivity;
    private MemberDAO memberDAO;
    private TeamDAO teamDAO;
    private String nom_aventure, nom_equipe;
    private int max;

    public TeamAdapter(List<TeamModel> listeTeam, int layout_id, Activity context) {
        this.layout_id = layout_id;
        this.listeTeam = listeTeam;
        this.mainActivity = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView TV_nom_equipe = super.itemView.findViewById(R.id.nom_team);
        public TextView TV_nom_aventure = super.itemView.findViewById(R.id.nom_aventure);
        public TextView TV_nbr_membre = super.itemView.findViewById(R.id.nombre_membre_team);
        public ViewHolder(View view){
            super(view);
        }

    }


    @NonNull
    @NotNull
    @Override
    public TeamAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(this.layout_id, parent, false);
        return new TeamAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TeamAdapter.ViewHolder holder, int position) {
        final TeamModel currentTeam = this.listeTeam.get(position);
        AventureDAO aventureDAO = LoginActivity.aventureDAO;
        teamDAO = LoginActivity.teamDAO;

        new Thread(
                ()-> {
                    AventureModel aventureModel = aventureDAO.getAventure(currentTeam.getId_aventure());

                    nom_aventure = "bug";
                    nom_aventure = aventureModel.getName();
                    max = aventureModel.getMaxGroup();

                    nom_equipe = "bug";
                    nom_equipe = teamDAO.getNomEquipe(currentTeam.getId());


                    if (!(holder.TV_nom_aventure == null)) {
                        holder.TV_nom_aventure.setText(nom_aventure);
                    }
                    if (!(holder.TV_nom_equipe == null)) {
                        holder.TV_nom_equipe.setText(nom_equipe);
                    }
                    if (!(holder.TV_nbr_membre == null)) {
                        String s = currentTeam.getNbr_membre() + "/" + max;
                        holder.TV_nbr_membre.setText(s);
                    }
                }
        ).start();




        holder.itemView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rejoindreUneEquipe(v, currentTeam.getId());
                    }}
        );
    }

    @Override
    public int getItemCount() {

        if (listeTeam != null){
            return listeTeam.size();
        }
        return 0;
    }

    public void rejoindreUneEquipe(View view, int id_equipe){
        new Thread(
                ()->{
                    TeamModel teamModel = teamDAO.getTeamAvecId(id_equipe);
                    memberDAO = LoginActivity.roomDatabase.memberDAO();
                    memberDAO.updateEquipe(LoginActivity.connectedMember.getId(), id_equipe);
                    int nbr_membre = teamModel.getNbr_membre();
                    teamDAO.updateNbrMembreEquipe(id_equipe, nbr_membre);
                    teamModel.setNbr_membre(nbr_membre+1);
                    LoginActivity.connectedMember.setEquipe(id_equipe);
                }
        ).start();


        Intent intent = new Intent(view.getContext(), MainActivity.class);
        mainActivity.setResult(Activity.RESULT_OK);
        view.getContext().startActivity(intent);
    }
}
