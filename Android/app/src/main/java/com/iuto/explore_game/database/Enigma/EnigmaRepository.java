package com.iuto.explore_game.database.Enigma;

import android.app.Application;
import com.iuto.explore_game.database.RoomDatabase;

import java.util.List;

public class EnigmaRepository {

    private EnigmaDAO enigmaDAO;
    private List<EnigmaModel> listeEnigmeAventure;

    public EnigmaRepository(Application application){
        RoomDatabase roomDatabase = RoomDatabase.getDatabase(application);
        enigmaDAO = roomDatabase.enigmaDAO();
    }


    public List<EnigmaModel> getListeEnigmeAventure(int id){
        return enigmaDAO.getEnigmesAventure(id);
    }



}
