package com.iuto.explore_game.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.iuto.explore_game.R;
import com.iuto.explore_game.database.Enigma.EnigmaModel;
import com.iuto.explore_game.ui.home.MenuRessourcePopUp;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RessourceAdapter extends RecyclerView.Adapter<RessourceAdapter.ViewHolder> {
    private final int layout_id;
    private final List<EnigmaModel> listeEnigma;
    private final Activity mainActivity;
    private int i;


    public RessourceAdapter(List<EnigmaModel> listeRessources, int layout_id, Activity activity) {
        this.layout_id = layout_id;
        this.listeEnigma = listeRessources;
        this.mainActivity = activity;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView nom = super.itemView.findViewById(R.id.nom_ressource);
        public TextView description = super.itemView.findViewById(R.id.description_ressource);
        public ViewHolder(View view){
            super(view);
        }
    }

    @NonNull
    @NotNull
    @Override
    public RessourceAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(this.layout_id, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RessourceAdapter.ViewHolder holder, int position) {
        final EnigmaModel currentEnigma = this.listeEnigma.get(position);
        i = listeEnigma.size() - position;
        if (!(holder.nom == null)) {
            holder.nom.setText("Enigme : " + currentEnigma.getId());
        }
        if (!(holder.description == null)) {
            holder.description.setText(currentEnigma.getDescription());
        }


        holder.itemView.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        new MenuRessourcePopUp(mainActivity, this, currentEnigma, i).show();
                    }}
        );

    }

    @Override
    public int getItemCount() {
        if (listeEnigma != null){
        return listeEnigma.size();}
        return 0;
    }

}