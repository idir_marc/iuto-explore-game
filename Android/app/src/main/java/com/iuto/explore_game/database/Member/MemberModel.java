package com.iuto.explore_game.database.Member;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import com.iuto.explore_game.database.Team.TeamModel;

@Entity(tableName = "memberModelTable", foreignKeys = @ForeignKey(entity = TeamModel.class, parentColumns = "id", childColumns = "id_equipe" ))
public class  MemberModel {
    public Integer id_equipe;

    @NonNull
    private String pseudo;
    private String role, mdp, mail;

    @PrimaryKey(autoGenerate = true)
    private int id;


    public MemberModel(String pseudo, String role, String mail,  String mdp, int id_equipe){
        this.pseudo = pseudo;
        this.role = "Joueur";
        this.mail = mail;
        this.mdp = mdp;
        this.id_equipe = null;
    }

    public MemberModel() {
        this.pseudo = "";
        this.role = "Joueur";
        this.mail = "";
        this.mdp = "";
    }


    public void setEquipe(Integer equipe) {
        this.id_equipe = equipe;
    }

    public Integer getEquipe() {
        return id_equipe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getRole() {
        return role;
    }

    public String getMail() {
        return mail;
    }

    public void setPseudo(String pseudo){
        this.pseudo = pseudo; }

    public void setRole(String role) {
        this.role = role; }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp){ this.mdp = mdp; }

    public void setMail(String mail){this.mail = mail;}

    @Override
    public String toString() {
        return "MemberModel{" +
                "pseudo='" + pseudo + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}