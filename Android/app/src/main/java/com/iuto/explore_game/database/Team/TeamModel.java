package com.iuto.explore_game.database.Team;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import com.iuto.explore_game.database.Aventure.AventureModel;
import com.iuto.explore_game.database.Member.MemberModel;

import java.util.List;

@Entity(tableName = "teamModelTable", foreignKeys = @ForeignKey(entity = AventureModel.class, parentColumns = "AdvId", childColumns = "id_aventure"))
public class TeamModel {

    private int num_current_enigma;

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    private String name;
    private int id_aventure, nbr_membre;

    public TeamModel(String name, int id_aventure, int nbr_membre, int num_current_enigma){
        this.name = name;
        this.id_aventure = id_aventure;
        this.nbr_membre = nbr_membre;
        this.num_current_enigma = num_current_enigma;
    }


    public int getNbr_membre() {
        return nbr_membre;
    }

    public void setNbr_membre(int nbr_membre) {
        this.nbr_membre = nbr_membre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_aventure() {
        return id_aventure;
    }

    public void setId_aventure(int id_aventure) {
        this.id_aventure = id_aventure;
    }

    public int getNum_current_enigma() {
        return num_current_enigma;
    }
    public void setNum_current_enigma(int num_current_enigma) {
        this.num_current_enigma = num_current_enigma;
    }
}
