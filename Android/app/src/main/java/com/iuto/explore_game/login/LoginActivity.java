package com.iuto.explore_game.login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.iuto.explore_game.ExempleBDLocal;
import com.iuto.explore_game.MainActivity;
import com.iuto.explore_game.database.Aventure.AventureDAO;
import com.iuto.explore_game.database.Enigma.EnigmaDAO;
import com.iuto.explore_game.database.Member.MemberModel;
import com.iuto.explore_game.R;
import com.iuto.explore_game.database.Member.MemberDAO;
import com.iuto.explore_game.database.RoomDatabase;
import com.iuto.explore_game.database.Team.TeamDAO;

public class LoginActivity extends AppCompatActivity {
    public static RoomDatabase roomDatabase;
    public static MemberModel user, connectedMember = null;
    private EditText pseudo, mail, mdp, username, password;
    public static  SharedPreferences sharedPreferences;
    private TextView tv;
    public static MemberDAO memberDAO;
    public static TeamDAO teamDAO;
    public static AventureDAO aventureDAO;
    public static EnigmaDAO enigmaDAO;
    private Boolean loginBon;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //memberViewModel = new ViewModelProvider(this).get(MemberViewModel.class);
        //deleteDatabase("member_database");
        roomDatabase = RoomDatabase.getDatabase(LoginActivity.this);
        //new ExempleBDLocal(); // cree un jeu de données pour la BD

        memberDAO = roomDatabase.memberDAO();
        teamDAO = LoginActivity.roomDatabase.teamDAO();
        aventureDAO = LoginActivity.roomDatabase.aventureDAO();
        enigmaDAO = LoginActivity.roomDatabase.enigmaDAO();


        // pour sauvegarder des infos meme quand on ferme l'appli

        sharedPreferences= this.getSharedPreferences("id_connected", this.MODE_PRIVATE);
        if(sharedPreferences!= null) {
            new Thread(
                    ()->{
                        Integer id_shared = sharedPreferences.getInt("id", 0);
                        MemberModel m = memberDAO.getMember(id_shared);
                        if (m != null ){
                            finish();
                            connectedMember = m;
                            Intent intent = new Intent(this, MainActivity.class);
                            startActivity(intent);
                        }
                    }
            ).start();
        }
    }

    public void signin(View view){
        MemberModel m = new MemberModel();
        m.setPseudo(pseudo.getText().toString());
        m.setMail(mail.getText().toString());
        m.setMdp(mdp.getText().toString());
        System.out.println(memberDAO);
        memberDAO.insert(m);
        connectedMember = m;
        doSave(view, m.getId());
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    public void login(View view){
        loginBon = null;
        new Thread(
                ()->{
                    username = findViewById(R.id.ET_login_username);
                    password = findViewById(R.id.ET_login_password);
                    user = LoginActivity.memberDAO.getMember(username.getText().toString(), password.getText().toString());

                    if (user != null ){
                        loginBon = true;
                        connectedMember = user;
                        finish();
                        doSave(view, connectedMember.getId());
                        Intent intent = new Intent(this, MainActivity.class);
                        intent.putExtra("username", username.getText().toString());
                        startActivity(intent);
                    }
                }
        ).start();

        tv = findViewById(R.id.commentaire_login);
        tv.setText("Pseudo ou mot de passe faux");
        }





    public void goSignin(View view){
        setContentView(R.layout.activity_signin);
        //elements pour recup info pour s'inscrire
        pseudo = findViewById(R.id.ET_signin_username);
        mail = findViewById(R.id.ET_signin_email);
        mdp = findViewById(R.id.ET_signin_password);
        //bouton pour s'inscrire
        Button button_signin = findViewById(R.id.button_signin);
        button_signin.setOnClickListener(
                new View.OnClickListener() {
                    @SuppressLint("ResourceType")
                    @Override
                    public void onClick(View v) {
                        new Thread(
                                ()->{
                                    signin(v);
                                }
                        ).start();
                    }}
        );
    }

    public void goLogin(View view){
        setContentView(R.layout.activity_login);
    }


    public void doSave(View view, int id)  {
        // The created file can only be accessed by the calling application
        // (or all applications sharing the same user ID).
        sharedPreferences = this.getSharedPreferences("id_connected", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("id",id);
        // Save.
        editor.apply();
    }
}