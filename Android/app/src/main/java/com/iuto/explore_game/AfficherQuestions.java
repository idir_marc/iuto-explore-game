package com.iuto.explore_game;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ActionMenuView;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.iuto.explore_game.adapter.QuestionAdapter;
import com.iuto.explore_game.adapter.TeamAdapter;
import com.iuto.explore_game.database.Enigma.EnigmaDAO;
import com.iuto.explore_game.database.Enigma.EnigmaModel;
import com.iuto.explore_game.database.Question.QuestionDAO;
import com.iuto.explore_game.database.Question.QuestionModel;
import com.iuto.explore_game.login.LoginActivity;

import java.util.ArrayList;
import java.util.List;

public class AfficherQuestions extends AppCompatActivity {
    private EnigmaDAO enigmaDAO;
    private EnigmaModel enigmaModel;
    private List<QuestionModel> listeQuestion;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.afficher_questions);
        Intent intent = getIntent();
        enigmaDAO = LoginActivity.roomDatabase.enigmaDAO();
        TextView TV_ressource_name = findViewById(R.id.afficherRessource_name);
        TextView TV_ressource_description = findViewById(R.id.afficherRessource_description);
        int id_enigme = intent.getIntExtra("id_enigme",0);

        Button BT_enregistrer = findViewById(R.id.button_valider_reponse);
        BT_enregistrer.setOnClickListener(
                new View.OnClickListener() {
                    @SuppressLint("ResourceType")
                    @Override
                    public void onClick(View v) {
                        new Thread(
                                ()->{




                                }
                        ).start();
                        finish();
                        Intent intent = new Intent(getApplication(), MainActivity.class);
                        startActivity(intent);
                    }}
        );


        Effect.buttonEffect(BT_enregistrer);

        Activity activity = this;

        new Thread(
                ()->{
                    QuestionDAO questionDAO = LoginActivity.roomDatabase.questionDAO();
                    listeQuestion = questionDAO.getQuestionsEnigma(id_enigme);
                    RecyclerView verticalRecyclerViewQuestion = findViewById(R.id.recycler_view_question);
                    verticalRecyclerViewQuestion.setAdapter(new QuestionAdapter(listeQuestion, R.layout.item_question, getParent()));
                   }
        ).start();

        new Thread(
                ()->{
                    enigmaModel = enigmaDAO.getEnigmeId(id_enigme);
                    TV_ressource_name.setText("Enigme : " + id_enigme);
                    TV_ressource_description.setText(enigmaModel.getDescription());
                }
        ).start();
    }

}
