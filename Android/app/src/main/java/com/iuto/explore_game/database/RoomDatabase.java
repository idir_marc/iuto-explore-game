package com.iuto.explore_game.database;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import com.iuto.explore_game.database.Aventure.AventureDAO;
import com.iuto.explore_game.database.Aventure.AventureModel;
import com.iuto.explore_game.database.Enigma.EnigmaDAO;
import com.iuto.explore_game.database.Enigma.EnigmaModel;
import com.iuto.explore_game.database.Member.MemberModel;
import com.iuto.explore_game.database.Member.MemberDAO;
import com.iuto.explore_game.database.Question.QuestionDAO;
import com.iuto.explore_game.database.Question.QuestionModel;
import com.iuto.explore_game.database.Team.TeamDAO;
import com.iuto.explore_game.database.Team.TeamModel;

@Database(entities = {MemberModel.class, AventureModel.class, TeamModel.class, EnigmaModel.class, QuestionModel.class}, version = 2)
public abstract class RoomDatabase extends androidx.room.RoomDatabase {
    private static RoomDatabase INSTANCE;

    public abstract MemberDAO memberDAO();
    public abstract AventureDAO aventureDAO();
    public abstract TeamDAO teamDAO();
    public abstract EnigmaDAO enigmaDAO();
    public abstract QuestionDAO questionDAO();

    public static RoomDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (RoomDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RoomDatabase.class, "member_database").build();
                }
            }
        }
        return INSTANCE;
    }
}

