package com.iuto.explore_game;

import android.util.DisplayMetrics;
import android.util.TypedValue;

public abstract class MetricsConvert {
    public static int dpToPx(float dp, DisplayMetrics metrics) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

    public static int spToPx(float sp, DisplayMetrics metrics) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, metrics);
    }

    public static int dpToSp(float dp, DisplayMetrics metrics) {
        return (int) (dpToPx(dp, metrics) / metrics.scaledDensity);
    }

    public static int spToDp(float sp, DisplayMetrics metrics) {
        return (int) (spToPx(sp, metrics) / metrics.scaledDensity);
    }
}
