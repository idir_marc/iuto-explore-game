package com.iuto.explore_game.ui.home;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.iuto.explore_game.AfficherQuestions;
import com.iuto.explore_game.Effect;
import com.iuto.explore_game.MainActivity;
import com.iuto.explore_game.R;
import com.iuto.explore_game.database.Enigma.EnigmaDAO;
import com.iuto.explore_game.database.Enigma.EnigmaModel;
import com.iuto.explore_game.login.LoginActivity;

public class MenuRessourcePopUp extends Dialog {
    private Activity activity;
    private View.OnClickListener listener;
    private EnigmaModel currentEnigme;
    private int position;

    public MenuRessourcePopUp(Activity activity, View.OnClickListener onClickListener, EnigmaModel enigmaModel, int position) {
        super(activity);
        this.position = position;
        this.activity = activity;
        this.listener = onClickListener;
        currentEnigme = enigmaModel;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_menu_ressource);
        setupComponents();

    }

    private void setupComponents() {

        TextView TV_nom_enigme = findViewById(R.id.popup_nom_enigme);
        TV_nom_enigme.setText("Enigme : " + currentEnigme.getId());

        Button button_questions = findViewById(R.id.button_questions_popup);
        Effect.buttonEffect(button_questions);
        button_questions.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, AfficherQuestions.class);
                        intent.putExtra("id_enigme",currentEnigme.getId());
                        activity.startActivity(intent);
                    }
                });


        Button button_ressource = findViewById(R.id.button_ressource_popup);
        Effect.buttonEffect(button_ressource);
        button_ressource.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

    }
}
