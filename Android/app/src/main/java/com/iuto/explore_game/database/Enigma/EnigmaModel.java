package com.iuto.explore_game.database.Enigma;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import com.iuto.explore_game.database.Aventure.AventureModel;

@Entity(tableName = "enigmaModelTable",foreignKeys = @ForeignKey(entity = AventureModel.class, parentColumns = "AdvId", childColumns = "id_adv"))
public class EnigmaModel {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    private int id_adv;
    private String localisationQR, indicLocalisationQR, description;

    public EnigmaModel(int id_adv, String localisationQR, String indicLocalisationQR, String description){
        this.id_adv = id_adv;
        this.localisationQR = localisationQR;
        this.indicLocalisationQR = indicLocalisationQR;
        this.description = description;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId_adv() {
        return id_adv;
    }

    public void setId_adv(int id_adv) {
        this.id_adv = id_adv;
    }

    public String getIndicLocalisationQR() {
        return indicLocalisationQR;
    }

    public void setIndicLocalisationQR(String indicLocalisationQR) {
        this.indicLocalisationQR = indicLocalisationQR;
    }

    public String getLocalisationQR() {
        return localisationQR;
    }

    public void setLocalisationQR(String localisationQR) {
        this.localisationQR = localisationQR;
    }
}
