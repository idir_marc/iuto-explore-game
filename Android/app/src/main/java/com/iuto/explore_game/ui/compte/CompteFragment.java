package com.iuto.explore_game.ui.compte;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.iuto.explore_game.*;
import com.iuto.explore_game.database.Member.MemberDAO;
import com.iuto.explore_game.database.Member.MemberModel;
import com.iuto.explore_game.databinding.FragmentCompteBinding;
import com.iuto.explore_game.login.LoginActivity;


public class CompteFragment extends Fragment {

    private CompteViewModel compteViewModel;
    private FragmentCompteBinding binding;
    private EditText ET_pseudo_compte, ET_mail_compte, ET_password;
    private MemberDAO memberDAO;
    private TextView TV_message_etat_enregistrement;
    private Boolean mdpBon;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        compteViewModel = new ViewModelProvider(this).get(CompteViewModel.class);
        binding = FragmentCompteBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        memberDAO = LoginActivity.memberDAO;
        MemberModel currentConnectedMember = LoginActivity.connectedMember;

        ET_pseudo_compte = root.findViewById(R.id.edit_pseudo_compte);
        ET_mail_compte = root.findViewById(R.id.edit_mail_compte);


        ET_pseudo_compte.setText(currentConnectedMember.getPseudo());
        ET_mail_compte.setText(currentConnectedMember.getMail());


        Button BT_enregistrer = root.getRootView().findViewById(R.id.button_enregistrer_changement_1);
        Effect.buttonEffect(BT_enregistrer);
        BT_enregistrer.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ET_pseudo_compte = root.findViewById(R.id.edit_pseudo_compte);
                        ET_mail_compte = root.findViewById(R.id.edit_mail_compte);
                        ET_password = root.findViewById(R.id.edit_password_compte);

                        String p = ET_pseudo_compte.getText().toString();
                        String m = ET_mail_compte.getText().toString();

                        new Thread(
                                ()->{
                                    String s = ET_password.getText().toString();
                                    if (LoginActivity.connectedMember.getMdp().equals(s)){
                                        memberDAO.updatePseudo(LoginActivity.connectedMember.getId(), p, m);
                                        LoginActivity.connectedMember.setPseudo(p);
                                        LoginActivity.connectedMember.setMail(m);
                                        getActivity().finish();
                                        Intent intent = new Intent(getActivity(), MainActivity.class);
                                        getActivity().startActivity(intent);
                                    }
                                }
                        ).start();

                        TV_message_etat_enregistrement = root.findViewById(R.id.message_etat_enregistrement);
                        TV_message_etat_enregistrement.setText("Mauvais mot de passe");



                    }}
        );





        //button deconnect
        Button BT_deconnect = root.getRootView().findViewById(R.id.button_deconnexion);
        Effect.buttonEffect(BT_deconnect);
        BT_deconnect.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        undoSave(v);
                        LoginActivity.connectedMember = null;

                        getActivity().finish();
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(intent);
                    }}
        );



        Button BT_signaler_probleme = root.getRootView().findViewById(R.id.button_signaler_probleme);
        Effect.buttonEffect(BT_signaler_probleme);
        BT_signaler_probleme.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new SignalerProblemePopUp(v.getContext(), this).show();
                    }}
        );



        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void undoSave(View view)  {
        // The created file can only be accessed by the calling application
        // (or all applications sharing the same user ID).
        SharedPreferences.Editor editor = LoginActivity.sharedPreferences.edit();
        editor.putInt("id",0);
        // Save.
        editor.apply();
    }

}