package com.iuto.explore_game.database.Team;

import android.app.Application;
import androidx.lifecycle.LiveData;
import com.iuto.explore_game.database.Member.MemberModel;
import com.iuto.explore_game.database.Member.MemberRepository;

import java.util.List;

public class TeamViewModel {
    private TeamRepository teamRepository;
    private TeamModel teamModel;
    private List<TeamModel> listeTeam;

    public TeamViewModel(Application application){
        teamRepository = new TeamRepository(application);
        teamModel = teamRepository.getTeamModel();
        listeTeam = teamRepository.getListeTeam();
    }

    public void insert(TeamModel teamModel){
        teamRepository.insert(teamModel);
    }

    public TeamModel getTeamModel() {
        return teamModel;
    }

    public List<TeamModel> getListeTeam() {
        return listeTeam;
    }
}
