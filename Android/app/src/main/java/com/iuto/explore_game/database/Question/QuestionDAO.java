package com.iuto.explore_game.database.Question;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface QuestionDAO {
    @Insert
    void insert(QuestionModel questionModel);

    @Query("select * from questionModelTable where enigma_id = :id")
    List<QuestionModel> getQuestionsEnigma(int id);

}
