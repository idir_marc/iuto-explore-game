package com.iuto.explore_game.database.Member;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

public class MemberViewModel extends ViewModel {
    private MemberRepository memberRepository;
    private LiveData<Integer> nbMembreLD;
    private LiveData<List<MemberModel>> allMembreLD;

    public MemberViewModel(Application application){
        memberRepository = new MemberRepository(application);
        nbMembreLD = memberRepository.getNbTasksLD();
        allMembreLD = memberRepository.getAllTasksLD();
    }

    public void delete(){
        memberRepository.deleteAll();
    }

    public void insert(MemberModel memberModel){
        memberRepository.insert(memberModel);
    }

    public LiveData<Integer> getNbTasksLD() {
        return nbMembreLD;
    }

    public LiveData<List<MemberModel>> getAllTaskLD() {
        return allMembreLD;
    }

}
