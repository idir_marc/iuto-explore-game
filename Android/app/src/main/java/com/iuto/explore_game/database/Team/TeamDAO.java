package com.iuto.explore_game.database.Team;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import com.iuto.explore_game.database.Aventure.AventureModel;
import com.iuto.explore_game.database.Member.MemberModel;

import java.lang.reflect.Member;
import java.util.List;

@Dao
public interface TeamDAO {

    @Insert
    void insert(TeamModel teamModel);

    @Query("DELETE from teamModelTable where id = :id")
    void deleteTeamAvecId(int id);

    @Query("SELECT * from teamModelTable  natural join MEMBERMODELTABLE where pseudo = :pseudo")
    TeamModel getTeam(String pseudo);

    @Query("SELECT * from teamModelTable where id = :id")
    TeamModel getTeamAvecId(int id);

    @Query("select * from teamModelTable")
    List<TeamModel> getAllTeam();



    @Query("select name from teamModelTable where id = :id")
    String getNomEquipe(int id);


    @Query("select id_aventure from teamModelTable where id = :id_equipe")
    int getIdAventure(int id_equipe);

    //update
    @Query("UPDATE teamModelTable SET nbr_membre = :nbr_membre WHERE id = :id_equipe")
    void updateNbrMembreEquipe(Integer id_equipe, int nbr_membre);

    @Query("select * from teamModelTable where name = :name and id_aventure = :id_aventure")
    TeamModel getTeamSansId(String name, int id_aventure);

}
