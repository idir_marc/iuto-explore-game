package com.iuto.explore_game.log;

import androidx.annotation.Nullable;

public interface LogSink {
    void log(
            Log.Priority priority,
            @Nullable String tag,
            @Nullable Throwable throwable,
            @Nullable String message
    );
}
