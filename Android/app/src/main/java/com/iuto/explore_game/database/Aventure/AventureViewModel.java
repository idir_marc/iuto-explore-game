package com.iuto.explore_game.database.Aventure;

import android.app.Application;
import androidx.lifecycle.LiveData;
import com.iuto.explore_game.database.Member.MemberModel;
import com.iuto.explore_game.database.Member.MemberRepository;

import java.util.List;

public class AventureViewModel {
    private AventureRepository aventureRepository;
    private List<AventureModel> listeAventure;

    public AventureViewModel(Application application){
        aventureRepository = new AventureRepository(application);
        listeAventure = aventureRepository.getAllAventure();
    }

    public void insert(AventureModel a){
        aventureRepository.insert(a);
    }

    public List<AventureModel> getListeAventure(){
        return this.listeAventure;
    }




}
