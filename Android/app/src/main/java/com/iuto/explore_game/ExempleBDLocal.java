package com.iuto.explore_game;

import com.iuto.explore_game.database.Aventure.AventureDAO;
import com.iuto.explore_game.database.Aventure.AventureModel;
import com.iuto.explore_game.database.Enigma.EnigmaDAO;
import com.iuto.explore_game.database.Enigma.EnigmaModel;
import com.iuto.explore_game.database.Member.MemberDAO;
import com.iuto.explore_game.database.Member.MemberModel;
import com.iuto.explore_game.database.Question.QuestionDAO;
import com.iuto.explore_game.database.Question.QuestionModel;
import com.iuto.explore_game.database.RoomDatabase;
import com.iuto.explore_game.database.Team.TeamDAO;
import com.iuto.explore_game.database.Team.TeamModel;
import com.iuto.explore_game.login.LoginActivity;

public class ExempleBDLocal {
    private TeamDAO teamDAO;
    private MemberDAO memberDAO;
    private EnigmaDAO enigmaDAO;
    private AventureDAO aventureDAO;
    private QuestionDAO questionDAO;
    private RoomDatabase roomDatabase;



    public ExempleBDLocal(){
        roomDatabase = LoginActivity.roomDatabase;
        creaInstance();
    }

    public void creaInstance(){
        teamDAO = LoginActivity.roomDatabase.teamDAO();
        memberDAO = LoginActivity.roomDatabase.memberDAO();
        enigmaDAO = LoginActivity.roomDatabase.enigmaDAO();
        aventureDAO = LoginActivity.roomDatabase.aventureDAO();
        questionDAO = LoginActivity.roomDatabase.questionDAO();
        new Thread(
                ()->{

                    AventureModel a1 = new AventureModel(1,2,6,"Monaco", "vivez monanco", "Monaco1",1);
                    AventureModel a2 = new AventureModel(2,2,6,"Litti", "vivez Litti", "Litti1",2);
                    AventureModel a3 = new AventureModel(3,2,6,"Central_Park", "c'est central park frr", "Central Park",3);
                    aventureDAO.insert(a1);
                    aventureDAO.insert(a2);
                    aventureDAO.insert(a3);

                    TeamModel t1 = new TeamModel("Ekip", 1, 2, 2);
                    TeamModel t2 =  new TeamModel("Ekip2", 3, 1, 0);
                    teamDAO.insert(t1);
                    teamDAO.insert(t2);

                    MemberModel membre1 = new MemberModel("Li", "Chef", "@gmail.com", "mdp", 1);
                    MemberModel membre2 =new MemberModel("as", "Joueur", "@gmail.com","mdp", 1);
                    MemberModel membre3 = new MemberModel("se", "Joueur", "@gmail.com","mdp", 2);
                    memberDAO.insert(membre1);
                    memberDAO.insert(membre2);
                    memberDAO.insert(membre3);

                    EnigmaModel e1 = new EnigmaModel(1, "Monaco", "CHERCHE MIEUX", "Les pigmé sont des hommes de petite taille vivant dans des grottes sous-marines prés de l'equateur.");
                    EnigmaModel e2 =new EnigmaModel(3, "Central_Park", "CHERCHE MIEUX", "Entré dans l'UNESCO, central park est un site ou des extraterrestres font souvent passage");
                    EnigmaModel e3 = new EnigmaModel(1, "Litti", "CHERCHE MIEUX", "Ca n'existe pas tu va rien apprendre en continuant a lire");
                    EnigmaModel e4 = new EnigmaModel(1, "Litti2", "CHERCHE MIEUX", "Cdzqdqzdqzdzqa n'existe pas tu va rien apprendre en continuant a lire");
                    EnigmaModel e5 = new EnigmaModel(1, "Litti3", "CHERCHE MIEUX", "htyhtjt-Ca n'existe pas tu va rien apprendre en continuant a lire");
                    EnigmaModel e6 = new EnigmaModel(1, "Litti4", "CHERCHE MIEUX", "t'Ca n'existe pas tu va rien apprendre en continuant a lire");
                    enigmaDAO.insert(e1);
                    enigmaDAO.insert(e2);
                    enigmaDAO.insert(e3);
                    enigmaDAO.insert(e4);
                    enigmaDAO.insert(e5);
                    enigmaDAO.insert(e6);


                    QuestionModel q1 = new QuestionModel(1, "checkbox", "2+2", "4", "2;3;6");
                    QuestionModel q2 = new QuestionModel(1, "checkbox", "2+2", "4", "56;489;6");
                    QuestionModel q3 = new QuestionModel(1, "text", "Quelle est la bonne ortographe ? yury ou yuri ?","yuri", "");
                    questionDAO.insert(q1);
                    questionDAO.insert(q2);
                    questionDAO.insert(q3);
                }
        ).start();
    }


}
