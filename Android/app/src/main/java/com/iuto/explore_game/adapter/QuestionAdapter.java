package com.iuto.explore_game.adapter;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.iuto.explore_game.R;
import com.iuto.explore_game.database.Aventure.AventureDAO;
import com.iuto.explore_game.database.Aventure.AventureModel;
import com.iuto.explore_game.database.Member.MemberDAO;
import com.iuto.explore_game.database.Question.QuestionDAO;
import com.iuto.explore_game.database.Question.QuestionModel;
import com.iuto.explore_game.database.Team.TeamDAO;
import com.iuto.explore_game.database.Team.TeamModel;
import com.iuto.explore_game.login.LoginActivity;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.ViewHolder> {
    private final int layout_id;
    private final List<QuestionModel> listeQuestions;
    private final Activity mainActivity;
    private TeamDAO teamDAO;
    private QuestionDAO questionDAO;
    private TeamAdapter.ViewHolder holder_changement;
    private String nom_aventure, nom_equipe;
    private int max;

    public QuestionAdapter(List<QuestionModel> listeQuestions, int layout_id, Activity context) {
        this.layout_id = layout_id;
        this.listeQuestions = listeQuestions;
        this.mainActivity = context;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView TV_question = super.itemView.findViewById(R.id.item_question_name);
        public CheckBox checkBox1 = super.itemView.findViewById(R.id.checkbox1);
        public CheckBox checkBox2 = super.itemView.findViewById(R.id.checkbox2);
        public CheckBox checkBox3 = super.itemView.findViewById(R.id.checkbox3);
        public CheckBox checkBox4 = super.itemView.findViewById(R.id.checkbox4);
        public EditText editText = super.itemView.findViewById(R.id.ET_reponse);
        public ViewHolder(View view) {
            super(view);
        }

    }


    @NonNull
    @NotNull
    @Override
    public QuestionAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(this.layout_id, parent, false);
        return new QuestionAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull QuestionAdapter.ViewHolder holder, int position) {
        final QuestionModel currentQuestion = this.listeQuestions.get(position);

        if (currentQuestion.getType().equals("checkbox")){
            holder.editText.setVisibility(View.INVISIBLE);

            String options = currentQuestion.getOptions();
            String[] options_split = options.split(";");

            List<String> list = new ArrayList<>();
            list.add(options_split[0]);
            list.add(options_split[1]);
            list.add(options_split[2]);
            list.add(currentQuestion.getAnswer());
            Collections.shuffle(list); // puis la trier aletoirement

            holder.checkBox1.setText(list.get(0));
            holder.checkBox2.setText(list.get(1));
            holder.checkBox3.setText(list.get(2));
            holder.checkBox4.setText(list.get(3));
        } else {
            holder.checkBox1.setVisibility(View.INVISIBLE);
            holder.checkBox2.setVisibility(View.INVISIBLE);
            holder.checkBox3.setVisibility(View.INVISIBLE);
            holder.checkBox4.setVisibility(View.INVISIBLE);


        }

        if (!(holder.TV_question == null)) {
            holder.TV_question.setText(currentQuestion.getText());
        }

        new Thread(
                () -> {
                }
        ).start();
    }

    @Override
    public int getItemCount() {
        return listeQuestions.size();

    }

}