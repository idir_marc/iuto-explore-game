package com.iuto.explore_game;

import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.navigation.NavOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.iuto.explore_game.adapter.MemberAdapter;
import com.iuto.explore_game.database.Member.MemberDAO;
import com.iuto.explore_game.database.RoomDatabase;
import com.iuto.explore_game.databinding.ActivityMainBinding;
import com.iuto.explore_game.log.Log;
import com.iuto.explore_game.log.LogSink;
import com.iuto.explore_game.login.LoginActivity;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    public Boolean estConnecte = false;
    private static NavController controller;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean debug = getResources().getBoolean(R.bool.debug);

        Log.addSink(new LogSink() {
            @Override
            public void log(Log.Priority priority, @Nullable String tag,
                            @Nullable Throwable throwable, @Nullable String message) {
                switch (priority) {
                    case DEBUG:
                        if (!debug) break;
                    case INFO:
                        System.out.printf("[%s](%s) %s\n", tag, priority.name, message);
                        if (throwable != null)
                            System.out.println(throwable.getStackTrace());
                        break;
                    case WARN:
                    case ERROR:
                        System.err.printf("[%s](%s) %s\n", tag, priority.name, message);
                        if (throwable != null)
                            throwable.printStackTrace();
                }
            }
        });

        if (LoginActivity.roomDatabase == null){
            LoginActivity.roomDatabase = RoomDatabase.getDatabase(MainActivity.this);
            MemberDAO memberDAO = LoginActivity.roomDatabase.memberDAO();
            new Thread(
                    ()->{
                        LoginActivity.connectedMember = memberDAO.getMember(3); // faire en sorte qu'il sauvegarde quelque part le bon truc
                    }
            ).start();

        }

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                    R.id.navigation_home, R.id.navigation_scan, R.id.navigation_messages, R.id.navigation_compte).build();

        controller = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, controller, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, controller);
    }

    public static boolean navigate(@IdRes int id, Bundle bundle, NavOptions options) {
        try {
            controller.navigate(id, bundle, options);
        } catch (IllegalStateException | IllegalArgumentException e) {
            return false;
        }
        return true;
    }
}
