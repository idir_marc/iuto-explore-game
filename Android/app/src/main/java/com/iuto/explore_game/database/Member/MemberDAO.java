package com.iuto.explore_game.database.Member;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MemberDAO {
    @Insert
    void insert(MemberModel memberModel);

    @Query("DELETE from memberModelTable")
    void deleteAllMembre();

    @Query("SELECT count(*) from memberModelTable")
    LiveData<Integer> nbMembersLD();

    @Query("SELECT * from memberModelTable")
    LiveData<List<MemberModel>> getAllMembersLD();

    @Query("SELECT * from memberModelTable where pseudo = :pseudo and mdp = :mdp")
    MemberModel getMember(String pseudo, String mdp);

    @Query("select * from memberModelTable where id = :id")
    MemberModel getMember(int id);

    @Query("select * from memberModelTable where id_equipe = :id_equipe ")
    List<MemberModel> getMemberTeam(int id_equipe);


    //update query


    //update equipe
    @Query("UPDATE memberModelTable SET id_equipe = :id_equipe WHERE id = :id")
    void updateEquipe(int id, Integer id_equipe);

    @Query("UPDATE memberModelTable SET pseudo = :pseudo, mail=:mail WHERE id = :id")
    void updatePseudo(int id, String pseudo, String mail);




}
