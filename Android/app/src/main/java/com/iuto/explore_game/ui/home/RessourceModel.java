package com.iuto.explore_game.ui.home;

public class  RessourceModel {
    final String nom;
    final String description;

    public RessourceModel(String nom, String description){
        this.nom = nom;
        this.description = description;
    }

    public String getNom() {
        return nom;
    }

    public String getDescription() {
        return description;
    }
}