package com.iuto.explore_game.ui.compte;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.iuto.explore_game.R;


public class SignalerProblemePopUp extends Dialog {
    private Context context;
    private View.OnClickListener listener;

    public SignalerProblemePopUp(Context contexte, View.OnClickListener onClickListener) {
        super(contexte);
        this.context = contexte;
        this.listener = onClickListener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_signaler_probleme);
        setupComponents();


    }

    private void setupComponents() {
        Button signal_button = findViewById(R.id.button_signaler_probleme);
        signal_button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

    }


}
