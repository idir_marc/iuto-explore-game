package com.iuto.explore_game.database.Question;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "questionModelTable")
public class QuestionModel {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    private int enigma_id;
    private String type, text, answer, options;

    public QuestionModel(int enigma_id, String type, String text, String answer, String options){
        this.enigma_id = enigma_id;
        this.type = type;
        this.text = text;
        this.answer = answer;
        this.options = options;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEnigma_id() {
        return enigma_id;
    }

    public void setEnigma_id(int enigma_id) {
        this.enigma_id = enigma_id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
