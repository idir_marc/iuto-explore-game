package com.iuto.explore_game.ui.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import com.iuto.explore_game.*;
import com.iuto.explore_game.adapter.TeamAdapter;
import com.iuto.explore_game.adapter.MemberAdapter;
import com.iuto.explore_game.adapter.RessourceAdapter;
import com.iuto.explore_game.database.Aventure.AventureDAO;
import com.iuto.explore_game.database.Aventure.AventureModel;
import com.iuto.explore_game.database.Enigma.EnigmaModel;
import com.iuto.explore_game.database.Member.MemberDAO;
import com.iuto.explore_game.database.Member.MemberModel;
import com.iuto.explore_game.database.Team.TeamDAO;
import com.iuto.explore_game.database.Team.TeamModel;
import com.iuto.explore_game.databinding.FragmentHomeBinding;
import com.iuto.explore_game.login.LoginActivity;

import java.util.List;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    private List<MemberModel> listeMembres;
    private List<RessourceModel> listeRessources;
    private List<EnigmaModel> listeEnigma;
    private List<TeamModel> listeTeam;
    private String nom_aventure, nom_equipe, description_aventure;
    private Activity activity;
    private Intent intent;
    private ViewGroup viewGroup;
    private TeamDAO teamDAO;
    private AventureDAO aventureDAO;
    private MemberDAO memberDAO;
    private List<String> liste_area, liste_aventure_from_area;


    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intent = super.getActivity().getIntent();
        activity = getActivity();

    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewGroup = container;
        teamDAO = LoginActivity.teamDAO;
        aventureDAO = LoginActivity.aventureDAO;
        memberDAO = LoginActivity.memberDAO;

        if (LoginActivity.connectedMember.getEquipe() != null){ //IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
            View root = inflater.inflate(R.layout.fragment_home, container, false);

            new Thread(
                    ()->{
                        int id_equipe = LoginActivity.connectedMember.getEquipe();
                        TeamModel teamModel = teamDAO.getTeamAvecId(id_equipe);
                        nom_equipe = teamModel.getName();
                        int id_adv = teamModel.getId_aventure();
                        nom_aventure = aventureDAO.getNomAventure(id_adv);
                        listeMembres = memberDAO.getMemberTeam(id_equipe);
                        description_aventure = aventureDAO.getDescription(id_adv);

                        //recyclerView pour les ressources et les membres
                        RecyclerView verticalRecyclerViewMember = root.getRootView().findViewById(R.id.vertical_recycler_view_member);
                        verticalRecyclerViewMember.setAdapter(new MemberAdapter(listeMembres, R.layout.item_member, root.getContext()));
                        int id_aventure = teamDAO.getIdAventure(id_equipe);
                        listeEnigma = aventureDAO.getEnigmaAventure(id_aventure);
                        int num_current_enigme = teamModel.getNum_current_enigma();
                        if (listeEnigma.size() >0){
                            listeEnigma = listeEnigma.subList(listeEnigma.size() - num_current_enigme,listeEnigma.size());
                        }

                        TextView TV_nom_team = root.findViewById(R.id.title_recyclerView_member);
                        TextView TV_nom_aventure = root.findViewById(R.id.aventure_name);
                        TextView TV_description_aventure = root.findViewById(R.id.aventure_description);

                        TV_nom_team.setText(nom_equipe);
                        TV_nom_aventure.setText(nom_aventure);
                        TV_description_aventure.setText(description_aventure);


                        
                        RecyclerView verticalRecyclerViewRessource = root.getRootView().findViewById(R.id.vertical_recycler_view_ressource);
                        verticalRecyclerViewRessource.setAdapter(new RessourceAdapter(listeEnigma, R.layout.item_ressource, activity));
                    }
            ).start();




            //quitter son equipe
            final Button leaveTeamButton = root.getRootView().findViewById(R.id.button_leave_team);
            leaveTeamButton.setOnClickListener(
                    new View.OnClickListener() {
                        @SuppressLint("ResourceType")
                        @Override
                        public void onClick(View v) {
                            viewGroup.removeView(root);
                            activity.finish();
                            new Thread(
                                    ()->{
                                        leaveTeam();
                                        Intent intent_vers_notTeam = new Intent(activity, MainActivity.class);
                                        startActivity(intent_vers_notTeam);

                                    }
                            ).start();
                        }}
            );

            return root;
        } else { //IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
            View v_notTeam = inflater.inflate(R.layout.fragment_home_not_team, container, false);
            Spinner spinner = v_notTeam.findViewById(R.id.spinner_area_aventure);


            //thread recycler view team
            new Thread(
                    ()->{
                        listeTeam = LoginActivity.teamDAO.getAllTeam();
                        RecyclerView verticalRecyclerViewTeam = v_notTeam.findViewById(R.id.vertical_recycler_view_teams);
                        verticalRecyclerViewTeam.setAdapter(new TeamAdapter(listeTeam, R.layout.item_team, activity));


                    }
            ).start();

            //thread spinner
            new Thread(
                    ()->{
                        liste_area = aventureDAO.getAllAventureArea();
                        if (liste_area.size() > 0){
                            liste_aventure_from_area = aventureDAO.getAllAventureFromArea(liste_area.get(0));
                        }

                        //set info des spinner
                        Spinner spinner2 = v_notTeam.findViewById(R.id.spinner_aventure_dispo_area);

                        //actualiser spinner aventure en fonction de l'area
                        spinner.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View view, MotionEvent motionEvent) {
                                new Thread(
                                        ()->{
                                            Spinner area_spinner = view.findViewById(R.id.spinner_area_aventure);
                                            liste_area = aventureDAO.getAllAventureArea();
                                            if (liste_area.size() > 0){
                                                //String s = spinner2.getSelectedItem().toString();
                                                liste_aventure_from_area = aventureDAO.getAllAventureFromArea(area_spinner.getSelectedItem().toString());
                                            }else {
                                                liste_aventure_from_area = new ArrayList<>();
                                                liste_aventure_from_area.add("La liste est vide");
                                            }}).start();
                                ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>
                                        (view.getContext(), android.R.layout.simple_spinner_item, liste_aventure_from_area);
                                //selected item will look like a spinner set from XML
                                spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner2.setAdapter(spinnerArrayAdapter2);

                                return false;
                            }
                        });

                        if (liste_area != null){
                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                                    (v_notTeam.getContext(), android.R.layout.simple_spinner_item, liste_area);

                            if (spinner != null && spinnerArrayAdapter != null){
                                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner.setAdapter(spinnerArrayAdapter);
                            }
                        }

                        if (liste_aventure_from_area != null) {
                            ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>
                                    (v_notTeam.getContext(), android.R.layout.simple_spinner_item, liste_aventure_from_area);
                            //selected item will look like a spinner set from XML
                            spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            if (spinner2 != null){
                                spinner2.setAdapter(spinnerArrayAdapter2);
                                spinner2.setPrompt("Choisissez une aventure");
                            }
                        }
                    }
            ).start();


            //creer son equipe
            Button button_creer_equipe = v_notTeam.findViewById(R.id.button_creer_equipe);
            button_creer_equipe.setOnClickListener(
                    new View.OnClickListener() {
                        @SuppressLint("ResourceType")
                        @Override
                        public void onClick(View v) {
                            new Thread(
                                    ()->{
                                        EditText ET_nom_equipe = v_notTeam.findViewById(R.id.ET_creer_equipe);
                                        String nom_equipe = ET_nom_equipe.getText().toString();

                                        Spinner spinnner_area = v_notTeam.findViewById(R.id.spinner_area_aventure);
                                        String area = "";
                                        if (spinnner_area != null){
                                            if (spinnner_area.getSelectedItem() != null){
                                                area = spinnner_area.getSelectedItem().toString();}
                                        }

                                        Spinner spinner_aventure = v_notTeam.findViewById(R.id.spinner_aventure_dispo_area);
                                        String aventure = "";
                                        if( spinner_aventure != null){
                                            if (spinner_aventure.getSelectedItem() != null){
                                                aventure = spinner_aventure.getSelectedItem().toString();}
                                        }
                                        int id_aventure = aventureDAO.getIdAvecNom(aventure);
                                        TeamModel createTeam = new TeamModel(nom_equipe, id_aventure,1,3);
                                        teamDAO.insert(createTeam);
                                        Intent intent = new Intent(activity, MainActivity.class);

                                        createTeam = teamDAO.getTeamSansId(createTeam.getName(), createTeam.getId_aventure());

                                        memberDAO.updateEquipe(LoginActivity.connectedMember.getId(), createTeam.getId());

                                        LoginActivity.connectedMember.setEquipe(createTeam.getId());
                                        startActivity(intent);
                                    }).start();
                        }
                    }
            );



        return v_notTeam;
        }
    }


        @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void leaveTeam(){
        MemberDAO memberDAO = LoginActivity.memberDAO;
        MemberModel memberModel = memberDAO.getMember(LoginActivity.connectedMember.getId());
        int id_equipe = memberModel.getEquipe();
        TeamModel teamModel = teamDAO.getTeamAvecId(id_equipe);
        LoginActivity.connectedMember.setEquipe(null);

        memberDAO.updateEquipe(LoginActivity.connectedMember.getId(), null);
        int nbr_membre = teamModel.getNbr_membre();
        teamModel.setNbr_membre(nbr_membre-1);
        teamDAO.updateNbrMembreEquipe(id_equipe, nbr_membre-1 );
        if (teamModel.getNbr_membre() <= 0){
            teamDAO.deleteTeamAvecId(id_equipe);
        }
    }
}