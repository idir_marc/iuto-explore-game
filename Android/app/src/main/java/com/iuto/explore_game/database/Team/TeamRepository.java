package com.iuto.explore_game.database.Team;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import com.iuto.explore_game.database.Member.MemberModel;
import com.iuto.explore_game.database.RoomDatabase;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TeamRepository {
    private TeamDAO teamDAO;
    private TeamModel teamModel;
    private List<TeamModel> listeTeam;

    public TeamRepository(Application application){
        RoomDatabase roomDatabase = RoomDatabase.getDatabase(application);
        teamDAO = roomDatabase.teamDAO();
        teamModel = teamDAO.getTeam("test");
        listeTeam = teamDAO.getAllTeam();
    }

    public TeamDAO getTeamDAO() {
        return teamDAO;
    }

    public List<TeamModel> getListeTeam() {
        return listeTeam;
    }

    public TeamModel getTeamModel() {
        return teamModel;
    }

    public void insert(TeamModel teamModel){
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                teamDAO.insert(teamModel);}}
        );
    }

}
