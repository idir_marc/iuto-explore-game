package com.iuto.explore_game.database.Question;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.room.PrimaryKey;
import com.iuto.explore_game.database.RoomDatabase;

import java.util.List;

public class QuestionRepository {
    private QuestionDAO questionDAO;
    private QuestionModel questionModel;

    public QuestionRepository(Application application){
        RoomDatabase roomDatabase = RoomDatabase.getDatabase(application);
        questionDAO = roomDatabase.questionDAO();
    }

    public List<QuestionModel> getQuestionsEnigma(int id){
        return questionDAO.getQuestionsEnigma(1);
    }


}
