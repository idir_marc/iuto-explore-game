package com.iuto.explore_game.database.Enigma;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface EnigmaDAO {

    @Insert
    void insert(EnigmaModel enigmaModel);

    @Query("select * from enigmaModelTable where id_adv = :id_adv")
    List<EnigmaModel> getEnigmesAventure(int id_adv);

    @Query("select * from enigmaModelTable where id = :id")
    EnigmaModel getEnigmeId(int id);



}
