package com.iuto.explore_game.database.Aventure;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import com.iuto.explore_game.database.Member.MemberModel;
import com.iuto.explore_game.database.RoomDatabase;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AventureRepository {
    private AventureDAO aventureDAO;
    List<AventureModel> listeAventure;

    public AventureRepository(Application application){
        RoomDatabase roomDatabase = RoomDatabase.getDatabase(application);
        aventureDAO = roomDatabase.aventureDAO();
        listeAventure = aventureDAO.getAllAventure();}

    public AventureDAO getAventureDAO(){
            return aventureDAO;
        }

    public void deleteAll(){
        aventureDAO.deleteAllAventure();
    }

    public List<AventureModel> getAllAventure(){
            return listeAventure;
        }

    public void insert(AventureModel aventureModel){
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                aventureDAO.insert(aventureModel);}}
        );
    }

}



