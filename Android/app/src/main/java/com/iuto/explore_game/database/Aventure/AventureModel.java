package com.iuto.explore_game.database.Aventure;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import com.iuto.explore_game.database.Team.TeamModel;

@Entity(tableName = "aventureModelTable")
public class AventureModel {

    @PrimaryKey(autoGenerate = true)
    private int AdvId;

    @NonNull
    private int minGroup, maxGroup;
    private String area, description, name;

    public AventureModel(int id, int min, int max, String area, String description, String name, int ownerId){
        this.AdvId = id;
        this.name = name;
        this.description = description;
        this.area = area;
        this.minGroup = min;
        this.maxGroup = max;
    }

    public AventureModel(){
        this.name = "name";
        this.description = "description";
        this.area = "area";
        this.minGroup = 1;
        this.maxGroup = 2;

    }

    public String getDescription() {
        return description;
    }

    public void setAdvId(int advId) {
        AdvId = advId;
    }

    public String getName() {
        return name;
    }

    public int getAdvId() {
        return AdvId;
    }

    public int getMaxGroup() {
        return maxGroup;
    }

    public int getMinGroup() {
        return minGroup;
    }



    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMaxGroup(int maxGroup) {
        this.maxGroup = maxGroup;
    }

    public void setMinGroup(int minGroup) {
        this.minGroup = minGroup;
    }

    public void setName(String name) {
        this.name = name;
    }

}

