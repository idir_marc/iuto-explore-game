package com.iuto.explore_game.database.Aventure;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import com.iuto.explore_game.database.Enigma.EnigmaModel;


import java.util.List;

@Dao
public interface AventureDAO {

    @Insert
    void insert(AventureModel aventureModel);

    @Query("DELETE from aventureModelTable")
    void deleteAllAventure();

    @Query("SELECT * from aventureModelTable")
    List<AventureModel> getAllAventure();

    @Query("select AdvId, name, description, area, maxGroup, minGroup  from teamModelTable natural join aventureModelTable where AventureModelTable.name = :nom_aventure " +
            "and AventureModelTable.area = :aera ")
    List<AventureModel> getAllAventureCurrent(String aera, String nom_aventure);

    @Query("select name from aventureModelTable where area=:area")
    List<String> getAllAventureFromArea(String area);

    @Query("select area from aventureModelTable")
    List<String> getAllAventureArea();

    @Query("select * from enigmaModelTable where id_adv = :id order by id desc")
    List<EnigmaModel> getEnigmaAventure(int id);


    @Query("select * from aventureModelTable where AdvId =  :id")
    AventureModel getAventure(int id);

    @Query("select name from aventureModelTable where AdvId =  :id")
    String getNomAventure(int id);

    @Query("select maxGroup from aventureModelTable where AdvId = :id_aventure")
    int getNombreMax(int id_aventure);

    @Query("select AdvId from aventureModelTable where name = :nom")
    int getIdAvecNom(String nom);

    @Query("select description from aventureModelTable where AdvId = :id")
    String getDescription(int id);


}
