package com.iuto.explore_game.database.Member;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import androidx.lifecycle.LiveData;
import com.iuto.explore_game.database.RoomDatabase;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MemberRepository {
    private MemberDAO memberDAO;
    private LiveData<Integer> nbTasksLD;
    private LiveData<List<MemberModel>> allTasksLD;

    public MemberRepository(Application appli){
        RoomDatabase roomDatabase = RoomDatabase.getDatabase(appli);
        memberDAO = roomDatabase.memberDAO();
        nbTasksLD = memberDAO.nbMembersLD();
        allTasksLD = memberDAO.getAllMembersLD();
    }

    public MemberDAO getMemberDAO() {
        return memberDAO;
    }

    public LiveData<Integer> getNbTasksLD() {
        return nbTasksLD;
    }

    public LiveData<List<MemberModel>> getAllTasksLD() {
        return allTasksLD;
    }

    public void deleteAll(){
        memberDAO.deleteAllMembre();
    }

    public void insert(MemberModel memberModel){
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                memberDAO.insert(memberModel);}}
        );
    }


}
