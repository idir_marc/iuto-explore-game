package com.iuto.explore_game.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.iuto.explore_game.database.Member.MemberModel;
import com.iuto.explore_game.R;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.ViewHolder> {
    private final int layout_id;
    private final List<MemberModel> listeMembres;
    private final Context MainActivity;


    public MemberAdapter(List<MemberModel> listeMembres, int layout_id, Context context) {
        this.layout_id = layout_id;
        this.listeMembres = listeMembres;
        this.MainActivity = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView pseudoMember = super.itemView.findViewById(R.id.nom_member);
        public ViewHolder(View view){
            super(view);
        }
    }



    @NonNull
    @NotNull
    @Override
    public MemberAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(this.layout_id, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MemberAdapter.ViewHolder holder, int position) {
        final MemberModel currentMembre = this.listeMembres.get(position);
        if (!(holder.pseudoMember == null)) {
            holder.pseudoMember.setText(currentMembre.getPseudo());
        }
    }

    @Override
    public int getItemCount() {
        return listeMembres.size();
    }
}