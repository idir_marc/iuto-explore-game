package com.iuto.explore_game.log;

import android.os.SystemClock;
import androidx.annotation.Nullable;

public class Logger {
    private static final Logger DEFAULT = new Logger();

    private String tag = null;

    public Logger(@Nullable String tag) {
        this.tag = tag;
    }

    private Logger() {}

    public void debug(@Nullable String message, @Nullable Throwable throwable) {
        Log.log(Log.Priority.DEBUG, tag, throwable, message);
    }

    public void info(@Nullable String message, @Nullable Throwable throwable) {
        Log.log(Log.Priority.INFO, tag, throwable, message);
    }

    public void warn(@Nullable String message, @Nullable Throwable throwable) {
        Log.log(Log.Priority.WARN, tag, throwable, message);
    }

    public void error(@Nullable String message, @Nullable Throwable throwable) {
        Log.log(Log.Priority.ERROR, tag, throwable, message);
    }

    public void measure(String message, Runnable runnable) {
        debug(String.format("⇢ %s",message),null);

        long start = SystemClock.elapsedRealtime();

        try {
            runnable.run();
        } finally {
            long took = SystemClock.elapsedRealtime() - start;
            debug(String.format("⇠ %s [%sms]",message, took),null);
        }
    }

    // Static (use DEFAULT)

    public static void defDebug(@Nullable String message, @Nullable Throwable throwable) {
        DEFAULT.debug(message, throwable);
    }

    public static void defInfo(@Nullable String message, @Nullable Throwable throwable) {
        DEFAULT.info(message, throwable);
    }

    public static void defWarn(@Nullable String message, @Nullable Throwable throwable) {
        DEFAULT.warn(message, throwable);
    }

    public static void defError(@Nullable String message, @Nullable Throwable throwable) {
        DEFAULT.error(message, throwable);
    }

    public static void defMeasure(String message, Runnable runnable) {
        DEFAULT.measure(message, runnable);
    }
}
