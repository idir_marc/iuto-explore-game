package com.iuto.explore_game.log;

import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Log {
    static Priority logLevel = Priority.DEBUG;

    private static final List<LogSink> sinks = new ArrayList<>();

    private static final boolean testMode = Objects.equals(System.getProperty("logging.test-mod"), "true");

    /**
     * Adds a sink that will receive log calls
     */
    public static void addSink(LogSink sink) {
        synchronized (sinks) {
            sinks.add(sink);
        }
    }

    /**
     * Low-level logging call.
     * @param priority the priority/type of this log message.
     *                 By default, DEBUG is used.
     * @param tag Used to identify the source of a log message.
     *            It usually identifies the class where the log
     *            call occurs.
     * @param throwable An exception to log.
     * @param message A message to be logged.
     */
    public static void log(
            Priority priority,
            @Nullable String tag,
            @Nullable Throwable throwable,
            @Nullable String message
    ) {
        if (priority.level >= logLevel.level) {
            synchronized (sinks) {
                sinks.forEach(sink -> sink.log(priority, tag, throwable, message));
            }
        }

        if (testMode) printTestModeMessage(priority, tag, throwable, message);
    }

    public static void log (
            @Nullable String tag,
            @Nullable Throwable throwable,
            @Nullable String message
    ) {
        log(Priority.DEBUG, tag, throwable, message);
    }

    @VisibleForTesting
    static void reset() {
        logLevel = Priority.DEBUG;
        synchronized (sinks) {
            sinks.clear();
        }
    }

    private static void printTestModeMessage(
            Priority priority,
            @Nullable String tag,
            @Nullable Throwable throwable,
            @Nullable String message
    ) {
        StringBuilder printMessage = new StringBuilder();
        printMessage.append(priority.name());
        printMessage.append(' ');
        if (tag != null) printMessage.append(String.format("[%s] ",tag));
        if (message != null) printMessage.append(message);

        System.out.println(printMessage);
        if (throwable != null) throwable.printStackTrace();
    }

    public enum Priority {
        DEBUG (android.util.Log.DEBUG, "Debug"),
        INFO  (android.util.Log.INFO, "Info"),
        WARN  (android.util.Log.WARN, "Warn"),
        ERROR (android.util.Log.ERROR, "Error");

        public final int level;
        public final String name;

        Priority(int level, String name) {
            this.level = level;
            this.name = name;
        }
    }
}
