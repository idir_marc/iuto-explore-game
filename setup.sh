#!/bin/bash

echo "=======================================";
echo " Explore Game data server setup wizard";
echo "=======================================";
echo;

echo "sudo required in order to setup Docker and MySQL server";
sudo -k;
if sudo true ; then
  echo "sudo granted. Continuing towards installation.";
else
  echo "sudo rights weren't validated. Exiting.";
  exit 1;
fi;

if [ ! -d "/usr/share/docker.io" ] ; then
  sudo apt install docker.io -y
else
  echo "Docker already installed. Continuing.";
fi;

choose() { echo ${1:RANDOM%${#1}:1} $RANDOM; }
pwd="$({ choose '!@#$%^&'
  choose '0123456789'
  choose 'abcdefghijklmnopqrstuvwxyz'
  choose 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  for i in $( seq 1 $(( 4 + RANDOM % 8 )) )
     do
        choose '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
     done
 } | sort -R | awk '{printf "%s",$1}')"



sudo docker pull mysql:5;
sudo docker run --name explore_game_db -e MYSQL_ROOT_PASSWORD=$pwd -p 5090:5090 -d mysql:5;
sudo docker cp database/db.sql explore_game_db:/;
com="mysql -u root -p < db.sql";
# echo $com;
echo;
echo "====================";
echo "Please enter the following password: $pwd";
sudo docker exec -it explore_game_db "/bin/sh" -c "$com" && echo "Database updated.";

# concerned file:' relative paths
files=("server/resources/conf.TOML" "www/conf.TOML");
for filename in ${files[*]} ; do
  echo -e "[database]\nu = \"root\"\np = \"$pwd\"" > $filename;
  # sed -i "s/password_go_here/$pwd/" $filename
done;
