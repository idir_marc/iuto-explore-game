DROP DATABASE IF EXISTS Explore_Game;
CREATE DATABASE Explore_Game;
USE Explore_Game;

DROP TABLE IF EXISTS Adventure;
CREATE TABLE IF NOT EXISTS Adventure (
	id		INTEGER NOT NULL,
	Advname	VARCHAR(100),
	description	VARCHAR(1000),
	icon		VARCHAR(100),
	name_creator	VARCHAR(100),
	PRIMARY KEY(id)
);
DROP TABLE IF EXISTS Adventures_teams;
CREATE TABLE IF NOT EXISTS Adventures_teams (
	adventure_id	INTEGER NOT NULL,
	team_id	INTEGER NOT NULL,
	PRIMARY KEY(adventure_id,team_id)
);
DROP TABLE IF EXISTS Enigma;
CREATE TABLE IF NOT EXISTS Enigma (
	id		INTEGER NOT NULL,
	adventure_id	INTEGER NOT NULL,
	locationIndic	VARCHAR(100),
	QRCodeLocation	VARCHAR(100),
	descrip	VARCHAR(200),
	PRIMARY KEY(id)
);
DROP TABLE IF EXISTS Question;
CREATE TABLE IF NOT EXISTS Question (
	id		INTEGER NOT NULL,
	enigma_id	INTEGER,
	type		VARCHAR(100),
	text		VARCHAR(100),
	answer		VARCHAR(100),
	options	VARCHAR(100),
	PRIMARY KEY(id)
);
DROP TABLE IF EXISTS Team;
CREATE TABLE IF NOT EXISTS Team (
	id		INTEGER NOT NULL,
	teamname	VARCHAR(100),
	PRIMARY KEY(id)
);
DROP TABLE IF EXISTS User;
CREATE TABLE IF NOT EXISTS User (
	username	VARCHAR(100) NOT NULL,
	mail		VARCHAR(100),
	role		VARCHAR(50),
	password	VARCHAR(100),
	PRIMARY KEY(username)
);
DROP TABLE IF EXISTS Users_team;
CREATE TABLE IF NOT EXISTS Users_team (
	user_id	VARCHAR(100) NOT NULL,
	team_id	INTEGER NOT NULL,
	PRIMARY KEY(user_id,team_id)
);



ALTER TABLE Adventure
ADD FOREIGN KEY(name_creator) REFERENCES User(username);

ALTER TABLE Adventures_teams
ADD FOREIGN KEY(adventure_id) REFERENCES Adventure(id);

ALTER TABLE Adventures_teams
ADD FOREIGN KEY(team_id) REFERENCES Team(id);

ALTER TABLE Enigma
ADD FOREIGN KEY(adventure_id) REFERENCES Adventure(id);

ALTER TABLE Question
ADD FOREIGN KEY(Team_id) REFERENCES Team(id);

ALTER TABLE Users_team
ADD FOREIGN KEY(user_id) REFERENCES User(username);

ALTER TABLE Users_team
ADD FOREIGN KEY(team_id) REFERENCES Team(id);
