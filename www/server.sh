#!/bin/bash

#################################
# Launcher for a local Flask app
# Code and tests: IDIR Marc
#################################

echo -ne "\033]0;Explore Game Server\007"

echo;
echo "Explore Game Website app";
echo "~~~~~~~~~~~~~~~~~~~";

if [ ! -d venv ]; then
  echo "";
  echo "Create a new virtualenv? (under ./venv ) [y/n]";
  read -r answer;
  case "$answer" in
    "y"|"Y" ) virtualenv -p python3 venv;
      source venv/bin/activate;
      pip install -r requirements.txt;;
    "n"|"N" ) echo "Exiting";exit 0;;
    * ) echo "Invalid";exit 0;;
  esac
  echo;
else
  source venv/bin/activate;
fi;

echo;

read -r -p "Choose server port > " -t 10 port;

if [ ! $? -eq 0 ]; then
  echo;
  port=5000;
fi;

echo;

if [ "$1" == "open" ]; then
  echo "Starting server for broadcast with port '$port'";
  echo;
  flask run -h 0.0.0.0 -p $(($port + 1 - 1));
else
  echo "Starting server locally on port '$port'";
  echo;
  flask run -p $(($port + 1 - 1));
fi;
