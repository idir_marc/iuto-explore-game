from io import TextIOBase
import click
from .app import app
from .models import *

@app.cli.command()
def loaddb():
    """
    Fabrique les tables et fait les inserts 
    """
    #suppression des anciennes tables
    db.drop_all()

    #creation des tables
    db.create_all()

    jules = newuser("jules", "cesar", "jules@cesar")
    theo = newuser("theo", "theo", "theo@theo")
    bertrand = newuser("bertrand", "bertrand", "bertrand@bertrand")
    eren = newuser("eren", "eren", "eren@eren")



    adventure1 = Adventure(Advname = "Parc des Buttes-Chaumont", 
    description = "Le parc des Buttes-Chaumont est un jardin public situé dans le nord-est de Paris, en France, dans le 19ᵉ arrondissement de la ville. Avec près de 25 hectares, le parc est l'un des plus grands espaces verts de Paris. ",
    icon = "buttes_chaumont.jpg",
    name_creator = "jules")

    adventure2 = Adventure(Advname = "Central Park", 
    description = "Central Park est le premier grand parc public à avoir été aménagé dans une ville américaine. Le projet d'un grand espace vert situé au cœur de New York ne faisait pas partie du Commissioners' Plan de 1811, grand plan cadastral à l'origine du découpage actuel des rues. En 1830, New York devient la plus grande ville des États-Unis, avec environ 200 000 habitants. Vers 1850, la plupart des New-Yorkais résident au sud de la 38e rue dans des quartiers surpeuplés et bruyants et les habitants ne disposent que de quelques espaces verts aménagés à l'époque, bien souvent des cimetières, comme le Green-Wood Cemetery à Brooklyn. ",
    icon = "central_park.jpg",
    name_creator = "jules")

    adventure3 = Adventure(Advname = "Kyōto Gyoen", 
    description = "Le Kyōto-gyoen (京都御苑) est un parc situé dans l'arrondissement de Kamigyō, au milieu de la ville de Kyoto au Japon. Il entoure le palais impérial de Kyoto. ",
    icon = "kyoto_gyoen.jpg",
    name_creator = "jules")

    adventure4 = Adventure(Advname = "Parc du Poutyl", 
    description = "Voici l’un des plus ravissants jardins publics de l’agglomération d’Orléans. En rive sud de la Loire, ce parc à la française (3,5 ha), d’une grande diversité botanique, dispose d’une terrasse surplombant le cours d’eau du Loiret : la vue plonge sur de belles villas et leurs gares à bateau, vestiges “Belle Époque” d’un cadre privilégié.",
    icon = "poutyl.JPG",
    name_creator = "theo")

    adventure5 = Adventure(Advname = "Parc Louis Pasteur", 
    description = "Ce grand jardin tourne autour des scènes végétales qui agrémentent les grandes pelouses. Vous pouvez découvrir les essences végétales, les monuments, les statues et les loisirs pour les enfants tels que les manèges, le théâtre de marionnettes ou le petit train.",
    icon = "louis_pasteur.jpg",
    name_creator = "bertrand")

    adventure6 = Adventure(Advname = "IUT Informatique", 
    description = "On y fait de l'info",
    icon = "IUT.png",
    name_creator = "eren")

    db.session.add(adventure1)
    db.session.add(adventure2)
    db.session.add(adventure3)
    db.session.add(adventure4)
    db.session.add(adventure5)
    db.session.add(adventure6)

    enigma1 = Enigma(adventure_id = adventure1.id,
    locationIndic= "000100010",
    QRCodeLocation = "Pres d'un arbre",
    descrip = "Un jour il c'est passer quelques choses ici"
    )

    enigma2 = Enigma(adventure_id = adventure1.id,
    locationIndic= "001101000",
    QRCodeLocation = "Pres d'une arche",
    descrip = "Cette arche a 64ans"
    )

    enigma3 = Enigma(adventure_id = adventure1.id,
    locationIndic= "100111010",
    QRCodeLocation = "Pres d'un rocher et d'un arbre",
    descrip = "Il y a trois arbres ici"
    )

    db.session.add(enigma1)
    db.session.add(enigma2)
    db.session.add(enigma3)
    question1 = Question(enigma_id = enigma1.id,
    type = "textfield",
    text = "De quel couleur est la status",
    answer = "Blanc",
    option = None
    )
    db.session.add(question1)
    enigma1.questions.append(question1)
    db.session.add(enigma1)
    adventure1.enigmas.append(enigma1)
    adventure1.enigmas.append(enigma2)
    adventure1.enigmas.append(enigma3)
    db.session.add(adventure1)
    db.session.commit()

    adventure1.concepteurs.append(jules)
    adventure2.concepteurs.append(jules)
    adventure3.concepteurs.append(jules)
    adventure4.concepteurs.append(theo)
    adventure5.concepteurs.append(bertrand)
    adventure6.concepteurs.append(eren)
    db.session.commit()


    