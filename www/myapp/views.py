from .app import UPLOAD_FOLDER, app, db, mkpath
from flask import render_template, url_for, redirect, request, flash
from .models import *
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, PasswordField, SelectField, SelectMultipleField, TextAreaField, EmailField
from wtforms.validators import DataRequired, length
from hashlib import sha256
from flask_login import login_user, current_user, logout_user, login_required
from werkzeug.utils import secure_filename


@app.route("/")
def home():
    return render_template('home.html',
                           title='Home',
                           adventures=get_adventure())


class AdventureForm(FlaskForm):
    id = HiddenField('id')
    Advname = StringField("Le nom de l'Aventure", validators=[DataRequired()])
    description = TextAreaField("La description", validators=[DataRequired()])
    next = HiddenField()


class SelectConcepteursForm(FlaskForm):
    id = HiddenField('id')
    concepteurs = SelectMultipleField('Concepteurs', validators=[DataRequired(), length(min=4, max=15)])


@app.route("/create/adventure", methods=("GET", "POST"))
@login_required
def create_adventure():
    f = AdventureForm()
    if f.is_submitted():
        if request.method == 'POST':
            # check if the post request has the file part
            if 'file' not in request.files:
                return redirect(url_for("adventure"))
            file = request.files['file']
            # If the user does not select a file, the browser submits an
            # empty file without a filename.
            if file.filename == '':
                # Pas de fichier 'image' sélectionné, une image par défaut a été utilisée
                fichier = file.filename = "default.png"
            elif allowed_file(file.filename):
                fichier = replace_space(file.filename)
                fichier = secure_filename(fichier)
                file.save(mkpath(UPLOAD_FOLDER + fichier))
            add_adventure(f.Advname.data, f.description.data, fichier, current_user.username)
            id = get_id_adventure_by_name(f.Advname.data)
            return redirect(url_for("adventure", id=id[0]))
    return render_template('create_adventure.html',
                           form=f)


@app.route("/edit/adventure-<int:id>", methods=("GET", "POST"))
@login_required
def edit_adventure(id):
    adventure = get_adventure_by_id(id)
    f = AdventureForm(id=id, Advname=adventure.Advname, description=adventure.description)
    return render_template(
        "edit_adventure.html",
        form=f,
        adventure=adventure,
        img=adventure.icon
    )


@app.route("/save/adventure", methods=("GET", "POST"))
@login_required
def save_adventure():
    a = None
    f = AdventureForm()
    id = int(f.id.data)
    a = get_adventure_by_id(id)
    a.Advname = f.Advname.data
    a.description = f.description.data
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            return redirect(url_for("edit_adventure", id=id))
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename != '' and file.filename != a.icon:  # si pas vide et si pas idem que précédente
            if allowed_file(file.filename):  # si fichier autorisé
                fichier = replace_space(file.filename)
                fichier = secure_filename(fichier)
                file.save(mkpath(UPLOAD_FOLDER + fichier))
                a.icon = fichier
    db.session.commit()
    return redirect(url_for("adventure", id=id))


@app.route("/adventure-<int:id>", methods=("GET", "POST"))
def adventure(id):
    adventure = get_adventure_by_id(id)
    taille = len(adventure.enigmas)
    return render_template('adventure.html',
                           adventure=adventure,
                           concepteurs=get_concepteur_adventure(id),
                           taille=taille
                           )


@app.route("/adventure-<int:id>/add/concepteur", methods=("GET", "POST"))
def add_concepteur(id):
    f = SelectConcepteursForm(id=id)
    f.concepteurs.choices = get_user_concepteur(id)
    if f.is_submitted():
        concepteurs = f.concepteurs.data
        for concepteur in concepteurs:
            add_concepteur_adventure(id, concepteur)
        return redirect(url_for("adventure", id=id))
    return render_template('add_concepteur.html',
                           form=f,
                           id=id
                           )


class EnigmaForm(FlaskForm):
    id = HiddenField('id', id="id_eni")
    adventure_id = HiddenField('id_adv', id="id_adv")
    locationIndic = StringField('Adresse du QRCode :', validators=[DataRequired()])
    QRCodeLocation = StringField('QRCode coordonnées :', validators=[DataRequired()])
    description = TextAreaField('Description :', validators=[DataRequired()])
    typeRessource = SelectField('Le type de la ressource :', choices=['Texte', 'Image', 'Audio', 'Video'])
    ressource = StringField('Le lien de la ressource :', validators=[DataRequired()])


@app.route("/adventure-<int:id_adv>/enigma-<int:id_eni>", methods=("GET", "POST"))
@login_required
def edit_enigma(id_adv, id_eni):
    enigma = get_enigma_by_id_adv(id_adv, id_eni)
    f = EnigmaForm(id=enigma.id, adventure_id=enigma.adventure_id, locationIndic=enigma.locationIndic,
                   QRCodeLocation=enigma.QRCodeLocation, description=enigma.descrip, typeRessource=enigma.typeRessource,
                   ressource=enigma.ressource)
    return render_template(
        "edit_enigma.html",
        form=f,
    )


@app.route("/adventure-<int:id>/add/enigma", methods=("GET", "POST"))
@login_required
def create_enigma(id):
    f = EnigmaForm(adventure_id=id)
    if f.is_submitted():
        if request.method == 'POST':
            add_enigma(id, f.QRCodeLocation.data, f.description.data, f.typeRessource.data, f.ressource.data)
        return redirect(url_for("adventure", id=id))
    return render_template(
        "create_enigma.html",
        form=f
    )


@app.route("/save/enigma", methods=("GET", "POST"))
@login_required
def save_enigma():
    e = None
    f = EnigmaForm()
    id = int(f.id.data)
    adventure_id = int(f.adventure_id.data)
    e = get_enigma_by_id_adv(adventure_id, id)
    e.locationIndic = "google.com/" + f.adventure_id.data + f.id.data
    e.QRCodeLocation = f.QRCodeLocation.data
    e.descrip = f.description.data
    e.typeRessource = f.typeRessource.data
    e.ressource = f.ressource.data
    db.session.commit()
    return redirect(url_for("adventure", id=adventure_id))


@login_required
@app.route("/adventure-<int:id_adv>/delete/enigma-<int:id_eni>", methods=("GET", "POST"))
def delete_enigma(id_adv, id_eni):
    delete_enigma_bd(id_adv, id_eni)
    return redirect(url_for("adventure", id=id_adv))


class QuestionForm(FlaskForm):
    id = HiddenField('id')
    enigma_id = HiddenField('enigma_id')
    type = SelectField('Le type de la question :', choices=['textfield', 'choicesbox'])
    text = StringField('La question :', validators=[DataRequired()])
    answer = StringField('La reponse (si plusieurs mettre un ; entre) :', validators=[DataRequired()])
    option = StringField("L'option de la question :")


@app.route("/adventure-<int:id_adv>/enigma-<int:id_eni>/question", methods=("GET", "POST"))
@login_required
def questions(id_adv, id_eni):
    enigma = get_enigma_by_id_adv(id_adv, id_eni)
    return render_template(
        "questions.html",
        enigma=enigma,
        id_adv=id_adv
    )


@app.route("/adventure-<int:id_adv>/enigma-<int:id_eni>/question-<int:id_question>", methods=("GET", "POST"))
@login_required
def edit_question(id_adv, id_eni, id_question):
    question = get_question_by_id_eni(id_eni, id_question)
    f = QuestionForm(id=question.id, enigma_id=question.enigma_id, type=question.type, text=question.text,
                     answer=question.answer, option=question.option)
    return render_template(
        "edit_question.html",
        form=f,
        id_adv=id_adv,
        id_eni=id_eni
    )


@app.route("/enigma-<int:id_eni>/add/question", methods=("GET", "POST"))
@login_required
def create_question(id_eni):
    f = QuestionForm(enigma_id=id_eni)
    id_adv = get_id_adv_by_id_enigma(id_eni)
    if f.is_submitted():
        if request.method == 'POST':
            add_question(id_eni, f.type.data, f.text.data, f.answer.data, f.option.data)
        return redirect(url_for("questions", id_adv=id_adv[0], id_eni=id_eni))
    return render_template(
        "create_question.html",
        form=f,
        id_adv=id_adv[0],
        id_eni=id_eni
    )


@app.route("/save/question", methods=("GET", "POST"))
@login_required
def save_question():
    q = None
    f = QuestionForm()
    id = int(f.id.data)
    enigma_id = int(f.enigma_id.data)
    id_adv = get_id_adv_by_id_enigma(enigma_id)
    print(id_adv)
    q = get_question_by_id_eni(enigma_id, id)
    q.text = f.text.data
    q.type = f.type.data
    q.answer = f.answer.data
    q.option = f.option.data
    db.session.commit()
    return redirect(url_for("questions", id_adv=id_adv[0], id_eni=enigma_id))


@app.route("/enigma-<int:id_eni>/delete/question-<int:id_question>", methods=("GET", "POST"))
@login_required
def delete_question(id_eni, id_question):
    id_adv = get_id_adv_by_id_enigma(id_eni)
    delete_question_bd(id_eni, id_question)
    return redirect(url_for("questions", id_adv=id_adv[0], id_eni=id_eni))


@app.route("/delete/adventure-<int:id_adv>", methods=("GET", "POST"))
@login_required
def delete_adventure(id_adv):
    delete_adventure_bd(id_adv)
    return redirect(url_for("home"))


class LoginForm(FlaskForm):
    username = StringField("Nom d'utilisateur")
    password = PasswordField('Mot de Passe')
    next = HiddenField()

    def get_authenticated_user(self):
        user = get_user(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None


class SignUpForm(FlaskForm):
    username = StringField("Nom d'utilisateur", validators=[DataRequired(), length(min=4, max=15)])
    mail = EmailField("Adresse mail", validators=[DataRequired(), length(min=4, max=30)])
    password = PasswordField('Mot de Passe', validators=[DataRequired(), length(min=4, max=15)])
    verifpassword = PasswordField('Vérifiez votre mot de passe',
                                  validators=[DataRequired(), length(min=4, max=15)])
    next = HiddenField()

    def get_authenticated_username(self):
        user = get_user(self.username.data)
        if user is None:
            return None
        else:
            return user


@app.route("/login", methods=("GET", "POST",))
def login():
    f = LoginForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    elif f.validate_on_submit():
        user = f.get_authenticated_user()
        if user is not None:
            next = f.next.data or url_for("home")
            if login_user(user):
                return redirect(next)
        else:
            flash("Ce pseudo n'existe pas ou le mot de passe est incorrect")
            redirect(url_for("login"))
    return render_template(
        "login.html",
        title="Connexion",
        titre="Connecte-toi pour plus d'avantages",
        form=f
    )


@app.route('/signup', methods=['POST', 'GET'])
def signup():
    f = SignUpForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    elif f.validate_on_submit():
        username = f.get_authenticated_username()
        if username:  # if a user is found, we want to redirect back to signup page so user can try again
            flash("Ce nom d'utilisateur existe déjà")
            return redirect(url_for("signup"))
        else:
            if f.verifpassword.data == f.password.data:
                new_user = newuser(f.username.data, f.password.data, f.mail.data)
                next = f.next.data or url_for("home")
                if new_user is not None:
                    login_user(new_user)
                    return redirect(next)
            return redirect(url_for("signup"))
    return render_template(
        "signup.html",
        title="Inscription",
        titre="Inscris-toi pour plus de fonctionnalités !",
        form=f
    )


@app.route("/logout", methods=['GET'])
@login_required
def logout():
    logout_user()
    return redirect(url_for("home"))
