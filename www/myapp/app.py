from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
import os.path
from flask_sqlalchemy import SQLAlchemy


app = Flask (__name__)
app.config["SECRET_KEY"] = "c963810b-643f-42ec-b807-5688e50e578a"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# app.config["SQLALCHEMY_DATABASE_URI"] = f"mssql+pyodbc:///root:password_go_here@127.0.0.1:5090/explore_game"

login_manager = LoginManager(app)
login_manager.login_view = "login"

Bootstrap(app)

def mkpath(p):
    return os.path.normpath(
            os.path.join(
            os.path.dirname(__file__),
            p))

app.config['SQLALCHEMY_DATABASE_URI']=('sqlite:///'+mkpath('../myapp.db'))


UPLOAD_FOLDER = './static/image/'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', "svg"}
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

db = SQLAlchemy(app)

# app.config['BOOTSTRAP_SERVE_LOCAL'] = True
