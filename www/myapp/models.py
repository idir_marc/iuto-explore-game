from sqlalchemy.orm import backref
from .app import app, db, login_manager, ALLOWED_EXTENSIONS
import random
from sqlalchemy.sql import func
from flask_login import UserMixin
from sqlalchemy import ForeignKey


class Question(db.Model):
    __tablename__ = 'question'
    id = db.Column(db.Integer, primary_key=True)
    enigma_id = db.Column(db.Integer, ForeignKey('enigma.id'))
    type = db.Column(db.String(100))
    text = db.Column(db.String(100))
    answer = db.Column(db.String(100))
    option = db.Column(db.String(100))

    # surchage toString
    def __repr__(self):
        return "<Id: %d, type: %s, text: %s, answer: %s, option: %s>" % (
        self.id, self.type, self.text, self.answer, self.option)


class Enigma(db.Model):
    __tablename__ = 'enigma'
    id = db.Column(db.Integer, primary_key=True)
    adventure_id = db.Column(db.Integer, ForeignKey('adventure.id'))
    locationIndic = db.Column(db.String(100))
    QRCodeLocation = db.Column(db.String(100))
    descrip = db.Column(db.String(200))
    typeRessource = db.Column(db.String(100))
    ressource = db.Column(db.String(200))
    questions = db.relationship("Question")

    # surchage toString
    def __repr__(self):
        return "<Id: %d, locationIndic: %s, QRCodeLocation: %s, descrip: %s>" % (
        self.id, self.locationIndic, self.QRCodeLocation, self.descrip)

    # création de l'association many-to-many entre Team et Adventure


association_adventures_teams = db.Table("adventures_teams",
                                        db.Column("adventure_id", db.Integer, db.ForeignKey("adventure.id"),
                                                  primary_key=True),
                                        db.Column("team_id", db.Integer, db.ForeignKey("team.id"), primary_key=True))

# création de l'association many-to-many entre User et Adventure
association_adventures_users = db.Table("adventures_users",
                                        db.Column("adventure_id", db.Integer, db.ForeignKey("adventure.id"),
                                                  primary_key=True),
                                        db.Column("user_id", db.String(100), db.ForeignKey("user.username"),
                                                  primary_key=True))

# création de l'association many-to-many entre User et Team
association_users_teams = db.Table("users_teams",
                                   db.Column("user_id", db.String(100), db.ForeignKey("user.username"),
                                             primary_key=True),
                                   db.Column("team_id", db.Integer, db.ForeignKey("team.id"), primary_key=True))


class Adventure(db.Model):
    __tablename__ = 'adventure'
    id = db.Column(db.Integer, primary_key=True)
    Advname = db.Column(db.String(100))
    description = db.Column(db.String(1000))
    icon = db.Column(db.String(100))
    name_creator = db.Column(db.String(100), ForeignKey('user.username'))
    enigmas = db.relationship("Enigma")
    concepteurs = db.relationship("User", secondary=association_adventures_users, lazy="dynamic",
                                  back_populates="adventures_concepteurs")
    teams = db.relationship("Team", secondary=association_adventures_teams, lazy="dynamic", back_populates="adventures")

    # surchage toString
    def __repr__(self):
        return "<Id: %d, Advname: %s, description: %s>" % (self.id, self.Advname, self.description)


class User(db.Model, UserMixin):
    __tablename__ = 'user'
    username = db.Column(db.String(100), primary_key=True)
    mail = db.Column(db.String(100), nullable=False, unique=True)
    abonnement = db.Column(db.Integer, nullable=False, default=0)
    role = db.Column(db.String(50))
    password = db.Column(db.String(100), nullable=False)
    adventures_create = db.relationship("Adventure")
    teams = db.relationship("Team", secondary=association_users_teams, lazy="dynamic", back_populates="users")
    adventures_concepteurs = db.relationship("Adventure", secondary=association_adventures_users, lazy="dynamic",
                                             back_populates="concepteurs")

    def get_id(self):
        return self.username

    # surchage toString
    def __repr__(self):
        return "<username: %s, mail: %s, role: %s>" % (self.username, self.mail, self.role)


class Team(db.Model):
    __tablename__ = 'team'
    id = db.Column(db.Integer, primary_key=True)
    teamname = db.Column(db.String(100))
    adventures = db.relationship("Adventure", secondary=association_adventures_teams, lazy="dynamic",
                                 back_populates="teams")
    users = db.relationship("User", secondary=association_users_teams, lazy="dynamic", back_populates="teams")

    # surchage toString
    def __repr__(self):
        return "<Id: %d, teamname: %s>" % (self.id, self.teamname)


db.create_all()


def get_user(username):
    return User.query.filter(User.username == username).one_or_none()


@login_manager.user_loader
def load_user(username):
    return User.query.get(username)


def newuser(username, password, mail):
    '''Adds a new user'''
    from .models import User
    from hashlib import sha256
    query = User.query.filter((User.username == username) & (User.mail == mail)).one_or_none()
    if query is None:
        m = sha256()
        m.update(password.encode())
        u = User(username=username, mail=mail, role="concepteur", password=m.hexdigest())
        db.session.add(u)
        db.session.commit()
        return u
    return None


def add_adventure(Advname, description, icon, user_name):
    """
    Ajoute une adventure
    """
    adv = Adventure.query.filter(Adventure.Advname == Advname).one_or_none()
    user = get_user(user_name)
    if adv is not None:
        return False
    adventure = Adventure(Advname=Advname, description=description, icon=icon, name_creator=user_name)
    db.session.add(adventure)
    db.session.commit()
    adventure.concepteurs.append(user)
    db.session.commit()
    return True


def get_id_Enigma(locationIndic):
    return Enigma.query.with_entities(Enigma.id).filter(Enigma.locationIndic == locationIndic).one_or_none()


def get_adventure():
    return Adventure.query.all()


def get_adventure_by_id(id):
    return Adventure.query.filter(Adventure.id == id).one()


def get_enigma_by_id(id):
    return Enigma.query.filter(Enigma.id == id).one()


def get_id_adv_by_id_enigma(id_eni):
    return Enigma.query.with_entities(Enigma.adventure_id).filter(Enigma.id == id_eni).one_or_none()


def get_enigma_by_id_adv(id_adv, id_eni):
    return Enigma.query.filter((Enigma.adventure_id == id_adv) & (Enigma.id == id_eni)).one_or_none()


def get_id_adventure_by_name(name):
    return Adventure.query.with_entities(Adventure.id).filter(Adventure.Advname == name).one_or_none()


def get_question_by_id_eni(id_eni, id_question):
    return Question.query.filter((Question.enigma_id == id_eni) & (Question.id == id_question)).one_or_none()


def get_concepteur_adventure(id):
    adv = Adventure.query.filter(Adventure.id == id).one()
    res = []
    for c in adv.concepteurs.all():
        res.append(c.username)
    return res


def get_concepteur():
    return User.query.filter(User.role == 'concepteur').all()


def get_user_concepteur(id):
    concepteurs = get_concepteur()
    adv_concepteurs = get_concepteur_adventure(id)
    res = []
    for user in concepteurs:
        if not (user.username in adv_concepteurs):
            res.append(user.username)
    return res


def add_concepteur_adventure(id, username):
    adventure = Adventure.query.filter(Adventure.id == id).one()
    user = get_user(username)
    adventure.concepteurs.append(user)
    db.session.commit()


def add_enigma(id_adv, QRCodeLocation, descrip, typeRessource, ressource):
    """
    Ajoute une enigme
    """
    enigma = Enigma(adventure_id=id_adv, QRCodeLocation=QRCodeLocation, descrip=descrip, typeRessource=typeRessource,
                    ressource=ressource)
    db.session.add(enigma)
    db.session.commit()
    adventure = get_adventure_by_id(id_adv)
    enigma.locationIndic = "google.com/" + str(enigma.adventure_id) + str(enigma.id)
    adventure.enigmas.append(enigma)
    db.session.commit()
    return True


def add_question(id_eni, type, text, answer, option):
    """
    Ajoute une question
    """
    question = Question(enigma_id=id_eni, type=type, text=text, answer=answer, option=option)
    db.session.add(question)
    enigma = get_enigma_by_id(id_eni)
    enigma.questions.append(question)
    db.session.commit()
    return True


def delete_question_bd(id_eni, id_question):
    enigma = get_enigma_by_id(id_eni)
    question = get_question_by_id_eni(id_eni, id_question)
    enigma.questions.remove(question)
    db.session.delete(question)
    db.session.commit()


def delete_enigma_bd(id_adv, id_eni):
    enigma = get_enigma_by_id_adv(id_adv, id_eni)
    for question in enigma.questions:
        delete_question_bd(id_eni, question.id)
    db.session.delete(enigma)
    db.session.commit()


def delete_adventure_bd(id_adv):
    adventure = get_adventure_by_id(id_adv)
    for enigma in adventure.enigmas:
        delete_enigma_bd(id_adv, enigma.id)
    db.session.delete(adventure)
    db.session.commit()


def allowed_file(filename):
    """
    Vérifie si le fichier comporte bien les extensions souhaitées
    """
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def replace_space(filename):
    """
    Remplace les espaces par des '_'
    """
    fichier = ""
    for l in filename:
        if l == " ":
            l = "_"
        fichier += l
    return fichier
