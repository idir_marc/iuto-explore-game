# from .constants import Constants
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

import logging

app = Flask(__name__, template_folder="../resources/templates", static_folder="../resources/static")

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)


app.config["SQLALCHEMY_DATABASE_URI"] = f"mssql+pyodbc:///root:password_go_here@127.0.0.1:5090/explore_game"

login_manager = LoginManager(app)
login_manager.login_view = "/?l=0"

db = SQLAlchemy(app)
