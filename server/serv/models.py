from sqlalchemy.orm import backref
from .app import app, db, login_manager
import random
from sqlalchemy.sql import func
from flask_login import UserMixin
from sqlalchemy import ForeignKey


class Question(db.Model):
    __tablename__ = 'question'
    id = db.Column(db.Integer, primary_key=True)
    enigma_id = db.Column(db.Integer, ForeignKey('enigma.id'))
    type = db.Column(db.String(100))
    text = db.Column(db.String(100))
    answer = db.Column(db.String(100))
    option = db.Column(db.String(100))

    # surchage toString
    def __repr__(self):
        return "<Id: %d, type: %s, text: %s, answer: %s, option: %s>" % (
        self.id, self.type, self.text, self.answer, self.option)


class Enigma(db.Model):
    __tablename__ = 'enigma'
    id = db.Column(db.Integer, primary_key=True)
    adventure_id = db.Column(db.Integer, ForeignKey('adventure.id'))
    locationIndic = db.Column(db.String(100))
    QRCodeLocation = db.Column(db.String(100))
    descrip = db.Column(db.String(200))
    questions = db.relationship("Question")

    # surchage toString
    def __repr__(self):
        return "<Id: %d, locationIndic: %s, QRCodeLocation: %s, descrip: %s>" % (
        self.id, self.locationIndic, self.QRCodeLocation, self.descrip)

    # création de l'association many-to-many entre User et Adventure


association_adventures_teams = db.Table("adventures_teams",
                                        db.Column("adventure_id", db.Integer, db.ForeignKey("adventure.id"),
                                                  primary_key=True),
                                        db.Column("team_id", db.Integer, db.ForeignKey("team.id"), primary_key=True))

# création de l'association many-to-many entre User et Team
association_users_teams = db.Table("users_teams",
                                   db.Column("user_id", db.String(100), db.ForeignKey("user.username"),
                                             primary_key=True),
                                   db.Column("team_id", db.Integer, db.ForeignKey("team.id"), primary_key=True))


class Adventure(db.Model):
    __tablename__ = 'adventure'
    id = db.Column(db.Integer, primary_key=True)
    Advname = db.Column(db.String(100))
    description = db.Column(db.String(1000))
    icon = db.Column(db.String(100))
    name_creator = db.Column(db.String(100), ForeignKey('user.username'))
    enigmas = db.relationship("Enigma")
    teams = db.relationship("Team", secondary=association_adventures_teams, lazy="dynamic", back_populates="adventures")

    # surchage toString
    def __repr__(self):
        return "<Id: %d, Advname: %s, description: %s>" % (self.id, self.Advname, self.description)


class User(db.Model, UserMixin):
    __tablename__ = 'user'
    username = db.Column(db.String(100), primary_key=True)
    mail = db.Column(db.String(100))
    role = db.Column(db.String(50))
    password = db.Column(db.String(100))
    adventures_create = db.relationship("Adventure")
    teams = db.relationship("Team", secondary=association_users_teams, lazy="dynamic", back_populates="users")

    def get_id(self):
        return self.username

    # surchage toString
    def __repr__(self):
        return "<username: %s, mail: %s, role: %s>" % (self.username, self.mail, self.role)


class Team(db.Model):
    __tablename__ = 'team'
    id = db.Column(db.Integer, primary_key=True)
    teamname = db.Column(db.String(100))
    adventures = db.relationship("Adventure", secondary=association_adventures_teams, lazy="dynamic",
                                 back_populates="teams")
    users = db.relationship("User", secondary=association_users_teams, lazy="dynamic", back_populates="teams")

    # surchage toString
    def __repr__(self):
        return "<Id: %d, teamname: %s>" % (self.id, self.teamname)
